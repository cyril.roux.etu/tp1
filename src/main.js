const data = [
    {
        name: 'Regina',
        base: 'tomate',
        price_small: 6.5,
        price_large: 9.95,
        image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
    },
    {
        name: 'Napolitaine',
        base: 'tomate',
        price_small: 6.5,
        price_large: 8.95,
        image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
    },
    {
        name: 'Spicy',
        base: 'crème',
        price_small: 5.5,
        price_large: 8,
        image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
    }
];

// data.sort(function(a,b) {
//     if(a.name < b.name) { return -1; }
//     if(a.name > b.name) { return 1; }
//     return 0;
// });
// data.sort(function(a,b){
//     let rep = a.price_small - b.price_small; 
//     if(rep==0){
//         return a.price_large-b.price_large;
//     } else return rep;
// });


//const data2 = data.filter(data => data.base=='tomate');
//const data2 = data.filter(data => data.price_small<6);

const data2 = data.filter(data => data.name.split('i').length>2);


let html = ``;
for(let i = 0; i < data2.length; i++){
    html = html + `<article class="pizzaThumbnail">
    <a href="${data[i].image}">
        <img src="${data2[i].image}" />
        <section>
            <h4>${data2[i].name}</h4>
            <ul>
                <li>Prix petit format : ${data2[i].price_small} €</li>
                <li>Prix grand format : ${data2[i].price_large} €</li>
            </ul>
        </section>
    </a>
</article>`;
}




//console.log(html);
document.querySelector('.pageContent').innerHTML = html;